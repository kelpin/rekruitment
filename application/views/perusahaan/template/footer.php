
		<ul class="m-nav-sticky" style="margin-top: 30px;">
			<li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Purchase" data-placement="left">
				<a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" target="_blank"><i class="la la-cart-arrow-down"></i></a>
			</li>
			<li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Documentation" data-placement="left">
				<a href="https://keenthemes.com/metronic/documentation.html" target="_blank"><i class="la la-code-fork"></i></a>
			</li>
			<li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Support" data-placement="left">
				<a href="https://keenthemes.com/forums/forum/support/metronic5/" target="_blank"><i class="la la-life-ring"></i></a>
			</li>
		</ul>
		<script src="<?=base_url('/asset/metronic')?>/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="<?=base_url('/asset/metronic')?>/assets2/demo/base/scripts.bundle.js" type="text/javascript"></script>
		<script src="<?=base_url('/asset/metronic')?>/assets2/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
		<script src="<?=base_url('/asset/metronic')?>/assets2/app/js/dashboard.js" type="text/javascript"></script>
	</body>

	<!-- end::Body -->
</html>